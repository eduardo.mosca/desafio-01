const { response } = require('express');
const express = require('express');

const app = express();
app.listen(8000);

let explorers = [];

app.use(express.json());

function verifica(req, res, next) {
  const {id} = req.params;
  const explorerIndex = explorers.findIndex(explorer => explorer.id === id);
  if(explorerIndex < 0)
  {
    return res.status(400).json({Error: "ID não encontrado"});
  }
  next();
}

app.get('/explorers', (req, res) => {
  res.json(explorers);
});

app.post('/explorers', (req, res) => {
  const {id, name} = req.body;
  const explorer = {id, name, stacks: []};
  explorers.push(explorer);
  res.json(explorer);
});

app.post('/explorers/:id/stacks', verifica, (req, res) => {
  const {id} = req.params;
  const stacks = req.body.name;
  const explorerIndex = explorers.findIndex(explorer => explorer.id === id);
  explorers[explorerIndex].stacks.push(stacks);
  const explorer = {
    stacks
  };
  explorer.stacks = stacks;
  res.json(explorer);
});

app.put('/explorers/:id', verifica, (req, res) => {
  const {id} = req.params;
  const {name} = req.body;
  const explorerIndex = explorers.findIndex(explorer => explorer.id === id);
  const explorer = {
    id,
    name
  };
  explorer.stacks = explorers[explorerIndex].stacks;
  explorers[explorerIndex] = explorer;

  return res.json(explorer);
});

app.delete("/explorers/:id", verifica, (req, res) => {
  const {id} = req.params;
  const explorerIndex = explorers.findIndex(explorer => explorer.id === id);
  explorers.splice(explorerIndex, 1);
  return res.status(204).send();
});